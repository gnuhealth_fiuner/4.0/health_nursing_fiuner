from trytond.pool import Pool

from . import health_nursing

from .report import nursing_report

from .wizard import wizard_create_nursing_reports
from .wizard import wizard_get_inpatient_medication
from .wizard import wizard_relate_patient_rounding


def register():
    Pool.register(
        health_nursing.AmbulatoryCare,
        health_nursing.PatientAmbulatoryCareMedicament,
        health_nursing.PatientAmbulatoryCareMedicalSupply,
        health_nursing.NProcedures,
        health_nursing.NursingProcedures,
        health_nursing.PatientRounding,
        health_nursing.PatientRoundingMedicament,
        health_nursing.PatientRoundingMedicalSupply,
        wizard_create_nursing_reports.CreateNursingReportsStart,
        wizard_get_inpatient_medication.GetInpatientMedicationStart,
        wizard_get_inpatient_medication.GetInpatientMedicationMedicament,
        module='health_nursing_fiuner', type_='model')
    Pool.register(
        nursing_report.NursingReport,
        module='health_nursing_fiuner', type_='report')
    Pool.register(
        wizard_create_nursing_reports.CreateNursingReportsWizard,
        wizard_get_inpatient_medication.GetInpatientMedicationWizard,
        wizard_relate_patient_rounding.RelatePatientRounding,
        module='health_nursing_fiuner', type_='wizard')
