from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.exceptions import UserError


class StockInformation():

    quantity_type = fields.Selection([
        ('units', "Units"),
        ('fractions', "Fractions")
        ], "Quantity type", sort=False, required=True)
    fraction = fields.Selection([
        (None, ''),
        ('1/2', "1/2"),
        ('1/4', "1/4"),
        ], "Fraction", sort=False,
        states={'invisible': Eval('quantity_type') == 'fractions'})
    numerator = fields.Integer("Numerator", help="Numerator",
            states={
                'invisible': Eval('quantity_type') == 'units',
                'required': Eval('quantity_type') == 'fractions'
                },
            domain=[
                ('numerator', 'in', [None, Eval('numerator_domain')])
                ])
    numerator_domain = fields.Function(
            fields.Integer('Numerator domain'),
            'on_change_with_numerator_domain')
    denominator = fields.Integer("Denominator", help="Denominator",
            states={
                'invisible': Eval('quantity_type') == 'units',
                'required': Eval('quantity_type') == 'fractions'
                },
            domain=[
                ('denominator', 'in', [None, Eval('denominator_domain')])
                ])
    denominator_domain = fields.Function(
            fields.Integer('Denominator domain'),
            'on_change_with_denominator_domain')
    lot_domain = fields.Function(
        fields.Many2Many('stock.lot', None, None, 'Stock lot'),
        'on_change_with_lot_domain')
    avaible = fields.Function(
        fields.Char('Avaible'),
        'on_change_with_avaible')
    quantity_string = fields.Function(
        fields.Char('Quantity'),
        'on_change_with_quantity_string')

    def _fraction_domain(self, field):
        number = getattr(self, field, None)
        if number and number > 0:
            return number
        return 1

    @fields.depends('numerator')
    def on_change_with_numerator_domain(self, name=None):
        return self._fraction_domain('numerator')

    @fields.depends('denominator')
    def on_change_with_denominator_domain(self, name=None):
        return self._fraction_domain('denominator')

    @fields.depends('quantity_type', 'quantity', 'fraction',
                    'numerator', 'denominator')
    def on_change_with_quantity_string(self, name=None):
        res = ''
        if self.quantity_type == 'units':
            if self.quantity:
                res = ' '.join([res, str(self.quantity)])
            if self.fraction:
                res = ' '.join([res, self.fraction])
        elif self.quantity_type == 'fractions' and \
            self.numerator and self.denominator:
            res = ' / '.join([str(self.numerator), str(self.denominator)])
        return res

    @staticmethod
    def default_quantity_type():
        return 'units'

    @classmethod
    def view_attributes(cls):
        return super().view_attributes() + [
            ('/form/group/group/separator[@id="divisor"]', 'states', {
                'invisible': Eval('quantity_type') == 'units'
            })]

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.lot.domain.append(('id', 'in', Eval('lot_domain')))
        cls.quantity.states['invisible'] = Eval('quantity_type') == 'fractions'
        cls.quantity.states['required'] = Eval('quantity_type') == 'units'


class StockMedicamentInformation(StockInformation):

    @fields.depends('medicament', 'quantity_type')
    def on_change_medicament(self, name=None):
        super().on_change_medicament()
        if self.medicament and self.medicament.name.anmat_fractionable:
            self.quantity_type ='fractions'
            self.denominator = self.medicament.name.anmat_fractions
        elif self.medicament:
            self.quantity_type = 'units'
            self.denominator = None
        self.numerator_domain = self.on_change_with_numerator_domain()
        self.denominator_domain = self.on_change_with_denominator_domain()

    def _get_avaible(self, location):
        pool = Pool()
        Product = pool.get('product.product')
        Lot = pool.get('stock.lot')

        lot = self.lot

        if self.medicament and location:
            location_id = location.id
            with Transaction().set_context(locations=[location_id]):
                quantity = 0
                if not lot:
                    for product in [x for x in self.medicament.name.template.products]:
                        product = Product(product.id)
                        quantity += product.quantity
                else:
                    quantity = Lot.get_quantity([lot], 'quantity')
                    if lot.id in quantity:
                        quantity = quantity[lot.id]
                    else:
                        quantity = '0.0'
                return str(quantity)
        return '0.0'

    def _get_lot_domain(self, location):
        pool = Pool()
        Lot = pool.get('stock.lot')
        lots = []
        if location and self.medicament:
            with Transaction().set_context(
                    locations=[location.id]):
                lots =Lot.search([
                    ('id', '>', 0),
                    ('quantity', '>', 0),
                    ('product', '=', self.medicament.name.id)
                    ])
                if lots:
                    res = [l.id for l in lots]
                    return res
        return lots

class StockSupplyInformation(StockInformation):

    @fields.depends('product', 'quantity_type')
    def on_change_product(self, name=None):
        if self.product and self.product.anmat_fractionable:
            self.quantity_type ='fractions'
            self.denominator = self.product.anmat_fractions
        else:
            self.quantity_type = 'units'
            self.denominator = None
        self.numerator_domain = self.on_change_with_numerator_domain()
        self.denominator_domain = self.on_change_with_denominator_domain()

    def _get_avaible(self, location):
        pool = Pool()
        Product = pool.get('product.product')
        Lot = pool.get('stock.lot')

        lot = self.lot

        if self.product and location:
            location_id = location.id
            with Transaction().set_context(locations=[location_id]):
                quantity = 0
                if not lot:
                    for product in [x for x in self.product.template.products]:
                        product = Product(product.id)
                        quantity += product.quantity
                else:
                    quantity = Lot.get_quantity([lot], 'quantity')
                    if lot.id in quantity:
                        quantity = quantity[lot.id]
                    else:
                        quantity = '0.0'
                return str(quantity)
        return '0.0'

    def _get_lot_domain(self, location):
        pool = Pool()
        Lot = pool.get('stock.lot')
        lots = []
        if location and self.product:
            with Transaction().set_context(
                    locations=[location.id]):
                lots =Lot.search([
                    ('id', '>', 0),
                    ('quantity', '>', 0),
                    ('product', '=', self.product.id)
                    ])
                if lots:
                    res = [l.id for l in lots]
                    return res
        return lots

class AmbulatoryCare(metaclass=PoolMeta):
    'Ambulatory Care'
    __name__ = 'gnuhealth.patient.ambulatory_care'

    STATES = {'readonly': Eval('state') == 'done'}

    weight = fields.Float('Weight',states = STATES)
    height = fields.Float('Height', states = STATES)
    cephalic_perimeter = fields.Float('Cephalic Perimeter', states = STATES)
    nursing_procedures = fields.Many2Many(
        'gnuhealth.nursing_procedure', 'name', 'n_procedure',
        'Procedures', states = STATES)

    bmi = fields.Float('BMI',states = STATES)

    @fields.depends('weight','height')
    def on_change_with_bmi(self):
        bmi = 0
        if self.weight and self.height:
            bmi = float(self.weight) / pow((float(self.height)/100),2)
        return float(bmi)

    @fields.depends('care_location', 'medicaments', 'medical_supplies')
    def on_change_care_location(self, name=None):
        if self.care_location:
            medicaments = self.medicaments
            for medicament in self.medicaments:
                medicament.care_location = self.care_location.id
                medicament.lot = medicament.on_change_with_lot()
                medicament.lot_domain = medicament.on_change_with_lot_domain()
                medicament.avaible = medicament.on_change_with_avaible()
            self.medicaments = medicaments

            supplies = self.medical_supplies
            for supply in self.medical_supplies:
                supply.care_location = self.care_location.id
                supply.lot = supply.on_change_with_lot()
                supply.lot_domain = supply.on_change_with_lot_domain()
                supply.avaible = supply.on_change_with_avaible()
            self.medical_supplies = supplies

    @classmethod
    def create_stock_moves(cls, ambulatory_cares, lines):
        pool = Pool()
        Move = pool.get('stock.move')
        Date = pool.get('ir.date')
        moves = []
        for ambulatory in ambulatory_cares:
            for medicament in lines['medicaments']:
                move_info = {}
                move_info['origin'] = str(ambulatory)
                move_info['product'] = medicament.medicament.name.id
                move_info['uom'] = medicament.medicament.name.default_uom.id
                if medicament.quantity_type == 'units':
                    quantity = medicament.quantity
                    quantity += 0.5 if medicament.fraction == "1/2" \
                            else 0.25 if medicament.fraction == "1/4" \
                            else 0
                    move_info['quantity'] = quantity
                elif medicament.quantity_type == 'fractions':
                    move_info['quantity'] = round(
                        medicament.numerator / medicament.denominator,
                        medicament.medicament.name.default_uom.digits)
                move_info['from_location'] = ambulatory.care_location.id
                move_info['to_location'] = \
                    ambulatory.patient.name.customer_location.id
                move_info['unit_price'] = \
                    medicament.medicament.name.list_price
                move_info['cost_price'] = \
                    medicament.medicament.name.cost_price
                if medicament.lot:
                    if medicament.lot.expiration_date and \
                            medicament.lot.expiration_date < Date.today():
                        raise UserError('Expired medicaments')
                    move_info['lot'] = medicament.lot.id
                moves.append(move_info)

            for medical_supply in lines['supplies']:
                move_info = {}
                move_info['origin'] = str(ambulatory)
                move_info['product'] = medical_supply.product.id
                move_info['uom'] = medical_supply.product.default_uom.id
                if medical_supply.quantity_type == 'units':
                    quantity = medical_supply.quantity
                    quantity += 0.5 if medical_supply.fraction == "1/2" \
                        else 0.25 if medical_supply.fraction == "1/4" \
                        else 0
                    move_info['quantity'] = quantity
                elif medical_supply.quantity_type == 'fractions':
                    move_info['quantity'] = round(
                        medical_supply.numerator / medical_supply.denominator,
                        medical_supply.product.default_uom.digits)
                move_info['from_location'] = ambulatory.care_location.id
                move_info['to_location'] = \
                    ambulatory.patient.name.customer_location.id
                move_info['unit_price'] = medical_supply.product.list_price
                move_info['cost_price'] = \
                    medical_supply.product.cost_price
                if medical_supply.lot:
                    if medical_supply.lot.expiration_date \
                            and medical_supply.lot.expiration_date \
                            < Date.today():
                        raise UserError('Expired supplies')
                    move_info['lot'] = medical_supply.lot.id
                moves.append(move_info)

        new_moves = Move.create(moves)
        Move.write(new_moves, {
            'state': 'done',
            'effective_date': Date.today(),
        })

        return True

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.medicaments.domain.append(
            ('care_location', '=', Eval('care_location')))
        cls.medical_supplies.domain.append(
            ('care_location', '=', Eval('care_location')))


class PatientAmbulatoryCareMedicament(
                StockMedicamentInformation, metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.ambulatory_care.medicament'

    care_location = fields.Many2One('stock.location', 'Care location')

    @fields.depends('medicament', 'care_location')
    def on_change_with_lot_domain(self, name=None):
        return self._get_lot_domain(self.care_location)

    @fields.depends('medicament', 'care_location')
    def on_change_with_lot(self, name=None):
        if self.medicament and self.care_location:
            lots = self._get_lot_domain(self.care_location)
            if len(lots) == 1:
                return lots[0]
        return None

    @fields.depends('medicament', 'care_location', 'lot')
    def on_change_with_avaible(self, name=None):
        return self._get_avaible(self.care_location)


class PatientAmbulatoryCareMedicalSupply(
                StockSupplyInformation, metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.ambulatory_care.medical_supply'

    care_location = fields.Many2One('stock.location', 'Care location')

    @fields.depends('care_location', 'product', 'lot')
    def on_change_with_avaible(self, name=None):
        return self._get_avaible(self.care_location)

    @fields.depends('care_location', 'product')
    def on_change_with_lot(self, name=None):
        if self.product and self.care_location:
            lots = self._get_lot_domain(self.care_location)
            if len(lots) == 1:
                return lots[0]
        return None

    @fields.depends('product', 'care_location')
    def on_change_with_lot_domain(self, name=None):
        return self._get_lot_domain(self.care_location)

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.lot.domain.append(('id', 'in', Eval('lot_domain')))


class PatientRounding(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.rounding'

    @classmethod
    @ModelView.button_action('health_nursing_fiuner.get_inpatient_medication_wizard')
    def get_inpatient_medication(cls, roundings):
        pass

    @fields.depends('hospitalization_location', 'medicaments', 'medical_supplies')
    def on_change_hospitalization_location(self, name=None):
        if self.hospitalization_location:
            medicaments = self.medicaments
            for medicament in self.medicaments:
                medicament.hospitalization_location = self.hospitalization_location.id
                medicament.lot = medicament.on_change_with_lot()
                medicament.lot_domain = medicament.on_change_with_lot_domain()
                medicament.avaible = medicament.on_change_with_avaible()
            self.medicaments = medicaments

            supplies = self.medical_supplies
            for supply in self.medical_supplies:
                supply.hospitalization_location = self.hospitalization_location.id
                supply.lot = supply.on_change_with_lot()
                supply.lot_domain = supply.on_change_with_lot_domain()
                supply.avaible = supply.on_change_with_avaible()
            self.medical_supplies = supplies

    @classmethod
    def create_stock_moves(cls, roundings, lines):
        pool = Pool()
        Move = pool.get('stock.move')
        Date = pool.get('ir.date')
        moves = []
        for rounding in roundings:
            for medicament in lines['medicaments']:
                move_info = {}
                move_info['origin'] = str(rounding)
                move_info['product'] = medicament.medicament.name.id
                move_info['uom'] = medicament.medicament.name.default_uom.id
                if medicament.quantity_type == 'units':
                    quantity = medicament.quantity
                    quantity += 0.5 if medicament.fraction == "1/2" \
                        else 0.25 if medicament.fraction == "1/4" \
                        else 0
                    move_info['quantity'] = quantity
                elif medicament.quantity_type == 'fractions':
                    move_info['quantity'] = round(
                        medicament.numerator / medicament.denominator,
                        medicament.medicament.name.default_uom.digits)
                move_info['from_location'] = \
                    rounding.hospitalization_location.id
                move_info['to_location'] = \
                    rounding.name.patient.name.customer_location.id
                move_info['unit_price'] = medicament.medicament.name.list_price
                move_info['cost_price'] = \
                    medicament.medicament.name.cost_price
                if medicament.lot:
                    if medicament.lot.expiration_date \
                            and medicament.lot.expiration_date < Date.today():
                        raise UserError('Expired medicaments')
                    move_info['lot'] = medicament.lot.id
                moves.append(move_info)
            for medical_supply in lines['supplies']:
                move_info = {}
                move_info['origin'] = str(rounding)
                move_info['product'] = medical_supply.product.id
                move_info['uom'] = medical_supply.product.default_uom.id
                if medical_supply.quantity_type == 'units':
                    quantity = medical_supply.quantity
                    quantity += 0.5 if medical_supply.fraction == "1/2" \
                        else 0.25 if medical_supply.fraction == "1/4" \
                        else 0
                    move_info['quantity'] = quantity
                elif medical_supply.quantity_type == 'fractions':
                    move_info['quantity'] = round(
                        medical_supply.numerator / medical_supply.denominator,
                        medical_supply.product.default_uom.digits)
                move_info['from_location'] = \
                    rounding.hospitalization_location.id
                move_info['to_location'] = \
                    rounding.name.patient.name.customer_location.id
                move_info['unit_price'] = medical_supply.product.list_price
                move_info['cost_price'] = \
                    medical_supply.product.cost_price
                if medical_supply.lot:
                    if medical_supply.lot.expiration_date \
                            and medical_supply.lot.expiration_date < \
                            Date.today():
                        raise UserError('Expired supplies')
                    move_info['lot'] = medical_supply.lot.id
                moves.append(move_info)
        new_moves = Move.create(moves)
        Move.write(new_moves, {
            'state': 'done',
            'effective_date': Date.today(),
            })

        return True


    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._buttons.update({
            'get_inpatient_medication': {
                'invisible': ~Eval('state').in_(['draft']),
                'depends': ['state']
                },
            })
        cls.medicaments.domain.append(
            ('hospitalization_location', '=',
                    Eval('hospitalization_location')))
        cls.medical_supplies.domain.append(
            ('hospitalization_location', '=',
                    Eval('hospitalization_location')))


class PatientRoundingMedicament(
                StockMedicamentInformation, metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.rounding.medicament'

    hospitalization_location = fields.Many2One('stock.location', 'Location')

    @fields.depends('medicament', 'hospitalization_location', 'lot')
    def on_change_with_avaible(self, name=None):
        return self._get_avaible(self.hospitalization_location)

    @fields.depends('medicament', 'hospitalization_location', 'product')
    def on_change_with_lot(self, name=None):
        if self.medicament:
            lots = self._get_lot_domain(self.hospitalization_location)
            if len(lots) == 1:
                return lots[0]
        return None

    @fields.depends('medicament', 'hospitalization_location', 'product')
    def on_change_with_lot_domain(self, name=None):
        return self._get_lot_domain(self.hospitalization_location)

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.lot.domain.append(('id', 'in', Eval('lot_domain')))


class PatientRoundingMedicalSupply(
                StockSupplyInformation, metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.rounding.medical_supply'

    hospitalization_location = fields.Many2One('stock.location', 'Location')

    @fields.depends('hospitalization_location', 'product', 'lot')
    def on_change_with_avaible(self, name=None):
        return self._get_avaible(self.hospitalization_location)

    @fields.depends('hospitalization_location', 'product')
    def on_change_with_lot(self, name=None):
        if self.product:
            lots = self._get_lot_domain(self.hospitalization_location)
            if len(lots) == 1:
                return lots[0]
        return None

    @fields.depends('hospitalization_location', 'product')
    def on_change_with_lot_domain(self, name=None):
        return self._get_lot_domain(self.hospitalization_location)

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.lot.domain.append(('id', 'in', Eval('lot_domain')))


class NProcedures(ModelSQL,ModelView):
    'Nursing Procedures'
    __name__='gnuhealth.n_procedure'

    code = fields.Char('Nursing Procedure Code', required=True)
    proc = fields.Char('Nursing Procedure')

    def get_rec_name(self, name):
        rec_name = self.code and self.code + ' 'or ''
        rec_name += self.proc and ' - ' + self.proc or ''
        return rec_name

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
                ('code',) + tuple(clause[1:]),
                ('proc',) + tuple(clause[1:]),
                ]

class NursingProcedures(ModelSQL,ModelView):
    ' Procedures'
    __name__ ='gnuhealth.nursing_procedure'

    name = fields.Many2One('gnuhealth.patient.ambulatory_care',
                'Session', ondelete='CASCADE')
    n_procedure = fields.Many2One('gnuhealth.n_procedure',
                'Procedures', ondelete='CASCADE')
