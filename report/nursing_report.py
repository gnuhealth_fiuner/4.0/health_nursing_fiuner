# -*- coding: utf-8 -*-

from trytond.pool import Pool
from trytond.report import Report
from trytond.transaction import Transaction

from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta

__all__ = ['NursingReport']

class NursingReport(Report):
    'Nursing Report'
    __name__ = 'nursing.reports'


    @classmethod
    def get_context(cls, records, data):
        pool = Pool()

        Atenciones = pool.get('gnuhealth.patient.ambulatory_care')    
        Procedimientos = pool.get('gnuhealth.n_procedure')
        Company = Pool().get('company.company')

        User = Pool().get('res.user')
        user = User(Transaction().user)
        #Moves = pool.get('stock.move')
        #Medicaments = pool.get('gnuhealth.medicament')
        
        context = super(NursingReport, cls).get_context(records, data)
        if data:
            start = data['start'] 
            end = data['end'] 
            atenciones = Atenciones.search([
                                                ('session_start','>=',start),
                                                ('session_start','<=',end),
                                                ('health_professional.institution.name.name','=',user.company.rec_name),
                                                ('state','=','done')
                                                ])
            procedimientos = Procedimientos.search([])

            context['start_date'] = start - timedelta(hours=3) 
            context['end_date'] = end - timedelta(hours=3)
            context['current_date'] = date.today()

            # calculo los totales por procedimiento, sexo y rango etario
            # creo un diccionario con todos los procedimientos disponibles
            context['totales_edad_sexo_mujeres'] = {}
            context['totales_edad_sexo_varones'] = {}
            context['totales_x_proc'] = {}
            
            # creo un diccionario con los codigo y las descripciones de los procedimientos
            context['nombres_procedimientos'] = {}

            # listado de codigos para poder acceder a los valores
            context['codigos'] = []

            for x in procedimientos:  
            	context['totales_edad_sexo_mujeres'][x.code] = [0,0,0,0,0,0,0,0,0]
            	context['totales_edad_sexo_varones'][x.code] = [0,0,0,0,0,0,0,0,0]
            	context['nombres_procedimientos'][x.code] = x.proc
            	context['codigos'].append(x.code)
                  
            context['totales_edad_sexo_mujeres']['TOTAL'] = [0,0,0,0,0,0,0,0,0,0]
            context['totales_edad_sexo_varones']['TOTAL'] = [0,0,0,0,0,0,0,0,0,0]

            # lleno los vectores con la cantidades totales por edad, sexo y procedimiento	
            for i in atenciones:
            	aux=i.nursing_procedures
            	for j in aux:
            		if i.patient.name.gender == 'f':
            			if i.patient.age_float < 1:
            				context['totales_edad_sexo_mujeres'][j.code][0] +=1
            				context['totales_edad_sexo_mujeres']['TOTAL'][0] +=1
            			elif i.patient.age_float >= 1 and i.patient.age_float < 5:
            				context['totales_edad_sexo_mujeres'][j.code][1] +=1
            				context['totales_edad_sexo_mujeres']['TOTAL'][1] +=1
            			elif i.patient.age_float >= 5 and i.patient.age_float < 10:
            				context['totales_edad_sexo_mujeres'][j.code][2] +=1
            				context['totales_edad_sexo_mujeres']['TOTAL'][2] +=1
            			elif i.patient.age_float >= 10 and i.patient.age_float < 15:
            				context['totales_edad_sexo_mujeres'][j.code][3] +=1
            				context['totales_edad_sexo_mujeres']['TOTAL'][3] +=1		
            			elif i.patient.age_float >= 15 and i.patient.age_float < 20:
            				context['totales_edad_sexo_mujeres'][j.code][4] +=1
            				context['totales_edad_sexo_mujeres']['TOTAL'][4] +=1	
            			elif i.patient.age_float >= 20 and i.patient.age_float < 25:
            				context['totales_edad_sexo_mujeres'][j.code][5] +=1
            				context['totales_edad_sexo_mujeres']['TOTAL'][5] +=1	
            			elif i.patient.age_float >= 25 and i.patient.age_float < 40:
            				context['totales_edad_sexo_mujeres'][j.code][6] +=1
            				context['totales_edad_sexo_mujeres']['TOTAL'][6] +=1	
            			elif i.patient.age_float >= 40 and i.patient.age_float < 65:
            				context['totales_edad_sexo_mujeres'][j.code][7] +=1
            				context['totales_edad_sexo_mujeres']['TOTAL'][7] +=1	
            			elif i.patient.age_float >= 65:
            				context['totales_edad_sexo_mujeres'][j.code][8] +=1
            				context['totales_edad_sexo_mujeres']['TOTAL'][8] +=1	   

            		if i.patient.name.gender == 'm':
            			if i.patient.age_float < 1:
            				context['totales_edad_sexo_varones'][j.code][0] +=1
            				context['totales_edad_sexo_varones']['TOTAL'][0] +=1
            			elif i.patient.age_float >= 1 and i.patient.age_float < 5:
            				context['totales_edad_sexo_varones'][j.code][1] +=1
            				context['totales_edad_sexo_varones']['TOTAL'][1] +=1
            			elif i.patient.age_float >= 5 and i.patient.age_float < 10:
            				context['totales_edad_sexo_varones'][j.code][2] +=1
            				context['totales_edad_sexo_varones']['TOTAL'][2] +=1
            			elif i.patient.age_float >= 10 and i.patient.age_float < 15:
            				print(i.patient.age_float)
            				context['totales_edad_sexo_varones'][j.code][3] +=1
            				context['totales_edad_sexo_varones']['TOTAL'][3] +=1		
            			elif i.patient.age_float >= 15 and i.patient.age_float < 20:
            				context['totales_edad_sexo_varones'][j.code][4] +=1
            				context['totales_edad_sexo_varones']['TOTAL'][4] +=1	
            			elif i.patient.age_float >= 20 and i.patient.age_float < 25:
            				context['totales_edad_sexo_varones'][j.code][5] +=1
            				context['totales_edad_sexo_varones']['TOTAL'][5] +=1	
            			elif i.patient.age_float >= 25 and i.patient.age_float < 40:
            				context['totales_edad_sexo_varones'][j.code][6] +=1
            				context['totales_edad_sexo_varones']['TOTAL'][6] +=1	
            			elif i.patient.age_float >= 40 and i.patient.age_float < 65:
            				context['totales_edad_sexo_varones'][j.code][7] +=1
            				context['totales_edad_sexo_varones']['TOTAL'][7] +=1	
            			elif i.patient.age_float >= 65:
            				context['totales_edad_sexo_varones'][j.code][8] +=1
            				context['totales_edad_sexo_varones']['TOTAL'][8] +=1	

            for x in procedimientos: 
                  context['totales_x_proc'][x.code] = [0,0]

            for x in context['codigos']:
                  for i in range(0,9):
                        context['totales_x_proc'][x][0] += context['totales_edad_sexo_mujeres'][x][i]
                        context['totales_x_proc'][x][1] += context['totales_edad_sexo_varones'][x][i]
                        context['totales_edad_sexo_mujeres']['TOTAL'][9] += context['totales_edad_sexo_mujeres'][x][i]
                        context['totales_edad_sexo_varones']['TOTAL'][9] += context['totales_edad_sexo_varones'][x][i]

            # obtengo los profesionles y las atenciones de cada uno 	
            context['totales_profesionales'] = {} 
            context['profesionales'] = []
           
            for x in atenciones:
            	prof = x.health_professional.name.lastname + ', ' + x.health_professional.name.name 
 
            	if prof in context['profesionales']:
            		pass
            		#context['totales_profesionales'][prof] += 1
            	else:
            		context['profesionales'].append(prof)		
            		context['totales_profesionales'][prof] = 0

            for x in atenciones:
            	prof = x.health_professional.name.lastname + ', ' + x.health_professional.name.name 
            	context['totales_profesionales'][prof] += 1

            # necesito los totales de pacientes por sexo y edad
            context['hombres'] = [0,0,0,0,0,0,0,0,0]
            context['mujeres'] = [0,0,0,0,0,0,0,0,0]	

            for i in atenciones:
            	if i.patient.name.gender == 'm':
            		if i.patient.age_float < 1:
            			context['hombres'][0] +=1
            		elif i.patient.age_float >= 1 and i.patient.age_float < 5:
            			context['hombres'][1] +=1
            		elif i.patient.age_float >= 5 and i.patient.age_float < 10:
            			context['hombres'][2] +=1
            		elif i.patient.age_float >= 10 and i.patient.age_float < 15:
            			context['hombres'][3] +=1
            		elif i.patient.age_float >= 15 and i.patient.age_float < 20:
            			context['hombres'][4] +=1
            		elif i.patient.age_float >= 20 and i.patient.age_float < 25:
            			context['hombres'][5] +=1
            		elif i.patient.age_float >= 25 and i.patient.age_float < 40:
            			context['hombres'][6] +=1	
            		elif i.patient.age_float >= 40 and i.patient.age_float < 65:
            			context['hombres'][7] +=1
            		elif i.patient.age_float >= 65:
            			context['hombres'][8] +=1

            	if i.patient.name.gender == 'f':
            		if i.patient.age_float < 1:
            			context['mujeres'][0] +=1
            		elif i.patient.age_float >= 1 and i.patient.age_float < 5:
            			context['mujeres'][1] +=1
            		elif i.patient.age_float >= 5 and i.patient.age_float < 10:
            			context['mujeres'][2] +=1
            		elif i.patient.age_float >= 10 and i.patient.age_float < 15:
            			context['mujeres'][3] +=1
            		elif i.patient.age_float >= 15 and i.patient.age_float < 20:
            			context['mujeres'][4] +=1
            		elif i.patient.age_float >= 20 and i.patient.age_float < 25:
            			context['mujeres'][5] +=1
            		elif i.patient.age_float >= 25 and i.patient.age_float < 40:
            			context['mujeres'][6] +=1	
            		elif i.patient.age_float >= 40 and i.patient.age_float < 65:
            			context['mujeres'][7] +=1
            		elif i.patient.age_float >= 65:
            			context['mujeres'][8] +=1	

            # totales por rango etario
            context['total_x_edad'] = [0,0,0,0,0,0,0,0,0,0]   
            context['totales_mujeres'] = 0
            context['totales_hombres'] = 0

            for x in range(0,9):
            	context['totales_hombres'] += context['hombres'][x]
            	context['totales_mujeres'] += context['mujeres'][x]	

            for x in range(0,9):
                  context['total_x_edad'][x] += context['hombres'][x] + context['mujeres'][x]
                  context['total_x_edad'][9] += context['hombres'][x] + context['mujeres'][x]

        return context


