from trytond.model import ModelView, fields
from trytond.wizard import Wizard, Button, StateView, StateTransition
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval
from ..health_nursing import StockMedicamentInformation

class GetInpatientMedicationStart(ModelView):
    'Get Inpatient Medication - Start'
    __name__ = 'gnuhealth.patient.rounding.get_medication.start'

    inpatient_medications = fields.Many2Many('gnuhealth.inpatient.medication',
                None, None, 'Medication Plan', readonly=True)
    medicaments = \
        fields.One2Many('gnuhealth.patient.rounding.get_medication.medicament',
                None, 'Medicaments')


class GetInpatientMedicationMedicament(ModelView, StockMedicamentInformation):
    'Get Inpatient Medication - Medicament'
    __name__ = 'gnuhealth.patient.rounding.get_medication.medicament'

    name = fields.Many2One('gnuhealth.patient.rounding', 'Ambulatory ID')
    medicament = fields.Many2One(
        'gnuhealth.medicament', 'Medicament',
        required=True)
    product = fields.Many2One('product.product', 'Product')
    quantity = fields.Integer('Quantity',
                states={
                    'invisible': Eval('quantity_type') == 'fractions',
                    'required': Eval('quantity_type') == 'units'
                    })
    fraction = fields.Selection([
        (None, ""),
        ('1/2', "1/2"),
        ('1/4', "1/4"),
        ], "Fraction", sort=False,
        states={'invisible': Eval('quantity_type') == 'fractions',})
    short_comment = fields.Char(
        'Comment',
        help='Short comment on the specific drug')
    lot = fields.Many2One(
        'stock.lot', 'Lot', depends=['lot_domain'],
        domain=[('id', 'in', Eval('lot_domain'))])
    avaible = fields.Char('Avaible', readonly=True)
    hospitalization_location = fields.Many2One('stock.location',
            'Hospitalization location')
    lot_domain = fields.Many2Many('stock.lot', None, None, 'Lot domain')

    def _fraction_domain(self, field):
        number = getattr(self, field, None)
        if number and number > 0:
            return number
        return 1

    @staticmethod
    def default_location():
        pool = Pool()
        PatientRounding = pool.get('gnuhealth.patient.rounding')
        transaction = Transaction()
        patient_rounding = PatientRounding(transaction.context['active_id'])
        if patient_rounding.hospitalization_location:
            return patient_rounding.hospitalization_location.id
        return None

    @fields.depends('medicament')
    def on_change_medicament(self):
        if self.medicament:
            self.product = self.medicament.name.id
        else:
            self.product = None

    def _get_avaible(self):
        pool = Pool()
        Product = pool.get('product.product')
        Lot = pool.get('stock.lot')

        lots = self._get_lot_domain()
        lot = None

        if len(lots) == 1:
            lot = Lot(lots[0])
        else:
            lot = self.lot

        if self.medicament and self.hospitalization_location:
            location_id = self.hospitalization_location.id
            with Transaction().set_context(locations=[location_id]):
                quantity = 0
                if not lot:
                    for product in [x for x in self.medicament.name.template.products]:
                        product = Product(product.id)
                        quantity += product.quantity
                else:
                    quantity = Lot.get_quantity([lot], 'quantity')
                    if lot.id in quantity:
                        quantity = quantity[lot.id]
                    else:
                        quantity = '0.0'
                return str(quantity)
        return '0.0'

    @fields.depends('hospitalization_location', 'medicament',
            'lot', 'product')
    def on_change_with_avaible(self, name=None):
        return self._get_avaible()

    def _get_lot_domain(self):
        pool = Pool()
        Lot = pool.get('stock.lot')
        lots = []
        if self.hospitalization_location and self.product:
            with Transaction().set_context(
                    locations=[self.hospitalization_location.id]):
                lots =Lot.search([
                    ('id', '>', 0),
                    ('quantity', '>', 0),
                    ('product', '=', self.product.id)
                    ])
                if lots:
                    res = [l.id for l in lots]
                    return res
        return lots

    @fields.depends('hospitalization_location', 'medicament', 'product')
    def on_change_with_lot(self, name=None):
        if self.medicament:
            lots = self._get_lot_domain()
            if len(lots) == 1:
                return lots[0]
        return None

    @fields.depends('hospitalization_location', 'medicament', 'product')
    def on_change_with_lot_domain(self, name=None):
        return self._get_lot_domain()

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.lot.domain.append(('id', 'in', Eval('lot_domain')))


class GetInpatientMedicationWizard(Wizard):
    'Get Inpatient Medication - Wizard'
    __name__ = 'gnuhealth.patient.rounding.get_medication.wizard'

    start = StateView('gnuhealth.patient.rounding.get_medication.start',
                'health_nursing_fiuner.get_inpatient_medication_start_form',[
                    Button('Cancel', 'end', 'tryton-cancel'),
                    Button('Done', 'put_medication', 'tryton-ok', default=True)
                    ])
    put_medication = StateTransition()

    def default_start(self, fields):
        pool = Pool()
        PatientRounding = pool.get('gnuhealth.patient.rounding')
        InpatientRegistration = pool.get('gnuhealth.inpatient.registration')
        Product = pool.get('product.product')

        patient_rounding = PatientRounding(Transaction().context.get('active_id'))
        hospitalization_location = patient_rounding.hospitalization_location

        inpatient_medications = []
        rounding_medications = []
        if patient_rounding.name and patient_rounding.name.medications:
            inpatient_registration = InpatientRegistration(patient_rounding.name.id)
            inpatient_medications = inpatient_registration.medications
            # Only active medications
            for medication in [med for med in inpatient_medications
                if med.is_active]:
                avaible = '0.0'
                if hospitalization_location:
                    location_id = hospitalization_location.id
                    with Transaction().set_context(locations=[location_id]):
                        quantity = 0
                        for product in medication.medicament.name.template.products:
                            product = Product(product.id)
                            quantity += product.quantity
                        avaible = str(quantity)
                rounding_medications.append({
                    'medicament': medication.medicament.id,
                    'quantity_type': medication.quantity_type,
                    'quantity': medication.qty,
                    'fraction': medication.fraction,
                    'numerator': medication.numerator,
                    'denominator': medication.denominator,
                    'numerator_domain': medication.numerator_domain,
                    'denominator_domain': medication.denominator_domain,
                    'quantity_string': medication.quantity_string,
                    'product': medication.medicament.name.id,
                    'avaible': avaible,
                    'hospitalization_location': hospitalization_location
                            and hospitalization_location.id,
                    })

        return {
            'inpatient_medications': [x.id for x in inpatient_medications if x.is_active],
            'medicaments': rounding_medications,
            }

    def transition_put_medication(self):
        pool = Pool()
        PatientRounding = pool.get('gnuhealth.patient.rounding')
        PatientRoundingMedication = pool.get('gnuhealth.patient.rounding.medicament')

        patient_rounding = PatientRounding(Transaction().context.get('active_id'))
        if patient_rounding.state == 'draft' and self.start.medicaments:
            for medication in self.start.medicaments:
                patient_rounding_medication = PatientRoundingMedication()
                patient_rounding_medication.name = patient_rounding.id
                patient_rounding_medication.medicament = \
                    medication.medicament.id
                patient_rounding_medication.quantity_type = medication.quantity_type
                patient_rounding_medication.quantity = medication.quantity
                patient_rounding_medication.fraction = medication.fraction
                patient_rounding_medication.numerator = medication.numerator
                patient_rounding_medication.numerator_domain = \
                    medication.numerator_domain
                patient_rounding_medication.denominator = \
                    medication.denominator
                patient_rounding_medication.denominator_domain = \
                    medication.denominator_domain
                patient_rounding_medication.product = medication.product.id
                patient_rounding_medication.save()
        return 'end'
