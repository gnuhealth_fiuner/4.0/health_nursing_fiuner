from trytond.wizard import Wizard, StateAction
from trytond.transaction import Transaction
from trytond.pool import Pool

class RelatePatientRounding(Wizard):
    "Relate Patient Rounding"
    __name__ = 'gnuhealth.patient.rounding.relate.wizard'

    start = StateAction('health_nursing.action_gnuhealth_patient_rounding_view')

    def do_start(self, action):
        pool = Pool()
        Rounding = pool.get('gnuhealth.patient.rounding')

        active_ids = Transaction().context.get('active_ids')
        rounding = Rounding.search([
                ('name.patient', 'in', active_ids)
                ])
        
        data = {}
        if rounding:
            data = {'res_id': [p.id for p in rounding]}
            if len(rounding) == 1:
                action['views'].reverse()
        return action, data

